

$(document).ready(function () {
    $(window).on("scroll", function () {
        if ($(window).scrollTop() > 100) {
            $(".filters").addClass("active");
        } else {
            $(".filters").removeClass("active");
        }
        scrollFunction();
    });
});

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("top_button").style.display = "flex";
    } else {
        document.getElementById("top_button").style.display = "none";
    }

}
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

var example1 = new Vue({
    el: '#games',
    data: {
        sort: true,
        arrow: 'fas fa-sort-alpha-down-alt',
        consoles: ["nintendo switch", "xbox 360", "super nintendo", "computer", "wii", "psx", "nintendo DS"],
        status: ["on hold", "playing", "finished", "plan to play"],
        games: games,
        search: '',
        search_name: "",
    },
    computed: {
        filteredItems() {
            if (this.sort) {
                return this.games.slice().sort(function (a, b) {
                    return (a.name > b.name) ? 1 : -1;
                }).filter(item => item.console.indexOf(this.search.toLowerCase()) > -1 || item.status.indexOf(this.search.toLowerCase()) > -1)
                    .filter(item => {
                        return item.name.toLowerCase().indexOf(this.search_name.toLowerCase()) > -1 ||
                            item.console.toLowerCase().indexOf(this.search_name.toLowerCase()) > -1 ||
                            item.status.toLowerCase().indexOf(this.search_name.toLowerCase()) > -1 ||
                            String(item.progress).toLowerCase().indexOf(this.search_name.toLowerCase()) > -1 ||
                            String(item.fullprogress).toLowerCase().indexOf(this.search_name.toLowerCase()) > -1;
                    });
            } else {
                return this.games.slice().sort(function (a, b) {
                    return (a.name < b.name) ? 1 : -1;
                }).filter(item => item.console.indexOf(this.search.toLowerCase()) > -1 || item.status.indexOf(this.search.toLowerCase()) > -1)
                    .filter(item => {
                        return item.name.toLowerCase().indexOf(this.search_name.toLowerCase()) > -1 ||
                            item.console.toLowerCase().indexOf(this.search_name.toLowerCase()) > -1 ||
                            item.status.toLowerCase().indexOf(this.search_name.toLowerCase()) > -1 ||
                            String(item.progress).toLowerCase().indexOf(this.search_name.toLowerCase()) > -1 ||
                            String(item.fullprogress).toLowerCase().indexOf(this.search_name.toLowerCase()) > -1;
                    });
            }
        },
        menuconsoles() {
            const newArr = this.consoles.map(value => { return { name: value } });
            return newArr;
        },
        menustatus() {
            const newArr = this.status.map(value => { return { name: value } });
            return newArr;
        }
    },
    methods: {
        filter(filtro) {
            this.search = filtro;
        },
        sortFunc() {
            this.sort = !this.sort;
            if (this.sort) {
                this.arrow = 'fas fa-sort-alpha-down-alt';
            } else { this.arrow = 'fas fa-sort-alpha-up' }
        }
    }

})

