const nintendoswitch = [
    {
        name: "Super Mario Oddisey",
        console: "nintendo switch",
        status: "on hold",
        img: "supermariooddisey.jpg",
        progress: 100,
        fullprogress: 60,
        stars: 5
    },
    {
        name: "Child of Light",
        console: "nintendo switch",
        status: "playing",
        img: "childoflight.webp",
        progress: 50,
        fullprogress: 50,
        stars: 5
    },
    {
        name: "The Legend of Zelda: Breath Of The Wild",
        console: "nintendo switch",
        status: "on hold",
        img: "zeldabreath.jpg",
        progress: 70,
        fullprogress: 20,
        stars: 5
    },
    {
        name: "Pokemon: Let's Go Pikachu!",
        console: "nintendo switch",
        status: "on hold",
        img: "letsgopikachu.webp",
        progress: 60,
        fullprogress: 20,
        stars: 4,
        timesplayed: 1
    },
    {
        name: "Yu-Gi-Oh! Legacy of the Duelist: Link Evolution",
        console: "nintendo switch",
        status: "playing",
        img: "yugilegacy.jpg",
        progress: 30,
        fullprogress: 10,
        stars: 4
    },
    {
        name: "Crash Bandicoot 4: It's About Time",
        console: "nintendo switch",
        status: "playing",
        img: "crash4.webp",
        progress: 20,
        fullprogress: 10,
        stars: 5
    },
];

const others = [

    {
        name: "Chrono Trigger",
        console: "super nintendo",
        status: "finished",
        img: "chrono.jpg",
        progress: 100,
        fullprogress: 50,
        stars: 5
    },
    {
        name: "Life is Strange",
        console: "xbox 360",
        status: "playing",
        img: "lifeisstrange.jpeg",
        progress: 60,
        fullprogress: 30,
        stars: 5
    },
    
    {
        name: "Alice Madness Returns",
        console: "xbox 360",
        status: "on hold",
        img: "alice.jpg",
        progress: 80,
        fullprogress: 70,
        stars: 5
    },
    {
        name: "Undertale",
        console: "computer",
        status: "finished",
        img: "undertale.jpg",
        progress: 100,
        fullprogress: 60,
        stars: 5
    },

    {
        name: "Stardew Valley",
        console: "computer",
        status: "playing",
        img: "stardew.png",
        progress: 80,
        fullprogress: 50,
        stars: 5
    },
    {
        name: "Yoshi's Island",
        console: "super nintendo",
        status: "finished",
        img: "yoshisisland.png",
        progress: 100,
        fullprogress: 90,
        stars: 5
    },
    {
        name: "The Legend of Zelda: A Link to the Past",
        console: "super nintendo",
        status: "plan to play",
        img: "zeldaalinktothepast.jpg",
        progress: 0,
        fullprogress: 0,
        stars: 0
    },

];

const games = [...nintendoswitch, ...others];